(ns server-side-cljs-react.core
  (:require-macros [secretary.core :refer [defroute]])
  (:import [goog History Uri]
           [goog.events EventType])
  (:require [reagent.core :as reagent]
            [secretary.core :as secretary]
            [goog.dom :as dom]
            [goog.events :as events]
            [goog.history.EventType :as HistoryEventType]))

(def app-state (reagent/atom {:page    nil
                              :counter 0}))

(enable-console-print!)

;; ============================================
;; Dynamic Browser Routing.

(defn hook-browser-navigation! []
  (let [history (doto (History.)
                      (events/listen
                       HistoryEventType/NAVIGATE
                       (fn [event]
                         (secretary/dispatch! (.-token event))))
                      (.setEnabled true))]
    ;; Thanks to https://gist.github.com/city41/aab464ae6c112acecfe1#gistcomment-1488078
    ;; We can transform "known routes" to the secretary Routes on the fly.
    (events/listen js/document "click"
               (fn [e]
                 (let [path (.getPath (.parse Uri (.-href (.-target e))))
                       title (.-title (.-target e))]
                   (when (secretary/locate-route path)
                     (. e preventDefault)
                     (. history (setToken path title))))))))


(defn app-routes []
  (secretary/set-config! :prefix "#")

  (defroute "/" []
    (swap! app-state assoc :page "home"))

  (defroute "/about" []
    (swap! app-state assoc :page "about"))

  (hook-browser-navigation!))

(declare home-page)
(declare about-page)
(defmulti current-page #(@app-state :page))
(defmethod current-page "home" []    [home-page])
(defmethod current-page "about" []   [about-page])
(defmethod current-page :default [] [:div "404 This page does not exist"])

;; ============================================
;; The actual Pages.

(defn home-page []
  [:div [:h2 "Welcome to reagent-server-rendering"]
   [:div
    [:span "You can inc the counter here ..."]
    [:button {:on-click #(swap! app-state update :counter inc)} (str (:counter @app-state))]]
   [:div [:a {:href "/about" :data-isomorph true} "go to about page"]]])

(defn about-page []
  [:div [:h2 "About reagent-server-rendering"]
   [:div "Did you know that the counter is currently at "
    [:strong (:counter @app-state)]]
   [:div [:a {:href "/" :data-isomorph true} "go to the home page"]]])

(def pages
  {"home"  home-page
   "about" about-page})

(defn ^:export render-page [seed]
  (let [seed* (js->clj seed :keywordize-keys true)]
    (reset! app-state seed*)
    (reagent/render-to-string [current-page])))

(defn ^:export main [page-id]
  ;; Configure dynamic Browser Routing
  (app-routes)
  ;; Initialy set the page to bootstrap
  (when page-id
    (reset! app-state (js->clj page-id :keywordize-keys true)))
    ;;(swap! app-state assoc :page (keyword page-id)))
  ;; Render the page.
  (reagent/render [current-page] (.getElementById js/document "app")))
