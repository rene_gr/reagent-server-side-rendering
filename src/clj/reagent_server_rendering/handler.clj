(ns reagent-server-rendering.handler
  (:require [aleph.flow :as flow]
            [aleph.http :as http]
            [clojure.java.io :as io]
            [hiccup.core :refer [html]]
            [hiccup.page :refer [include-js include-css]]
            [compojure.core :refer [GET defroutes]]
            [compojure.route :refer [not-found resources]]
            [ring.middleware.defaults :refer [site-defaults wrap-defaults]]
            [cheshire.core :refer [generate-string]]
            [environ.core :refer [env]])
  (:import [io.aleph.dirigiste Pools]
           [javax.script ScriptEngineManager Invocable])
  (:gen-class))

;; Function to create the Nashorn JavaScript engine and render reagent components
(defn- create-js-engine []
 (doto (.getEngineByName (ScriptEngineManager.) "nashorn")
   ; React requires either "window" or "global" to be defined.
   (.eval "var global = this")
   (.eval (-> "public/js/compiled/app.js"
              io/resource
              io/reader))))


; We have one and only one key in the pool, it's a constant.
(def ^:private js-engine-key "js-engine")
(def ^:private js-engine-pool
  (flow/instrumented-pool
    {:generate   (fn [_] (create-js-engine))
     :controller (Pools/utilizationController 0.9 10000 10000)}))

(defn- render-page [seed]
  (let [js-engine @(flow/acquire js-engine-pool js-engine-key)]
    (try (.eval js-engine "console = {
                              log: print,
                              warn: print,
                              error: print
                          };")
         (.eval js-engine (str "server_side_cljs_react.core.render_page.call(null, " seed ")"))
         (finally (flow/release js-engine-pool js-engine-key js-engine)))))

(defn page
  "The render-page function will acquire an engine from the pool and call the
  render-page function from the *reagent-server-rendering.core* ClojureScript
  namespace. It will pass this function the page id as the argument.
  Once the page is rendered the JavaScript engine is released back into
  the pool."
  [page-id]
  (let [seed {:page    page-id
              :counter 4}]
   (html
    [:html
     [:head
      [:meta {:charset "utf-8"}]
      [:meta {:name "viewport"
              :content "width=device-width, initial-scale=1"}]
      (include-css "css/site.css")]
     [:body
      [:div#app
       (render-page (generate-string seed))]
      (include-js "js/compiled/app.js")
      [:script {:type "text/javascript"}
       (str "server_side_cljs_react.core.main(" (generate-string seed) ");")]]])))


(defroutes app-routes
  (GET "/" [] (page :home))
  (GET "/about" [] (page :about))
  (resources "/")
  (not-found "Not Found"))

(def app (wrap-defaults app-routes site-defaults))

(defn -main [& [port]]
  (let [port (Integer. (or port (env :port) 10555))]
    (println (str "Server will start on port " port " ..."))
    (http/start-server app {:port port})))
