(defproject server-side-cljs-react "0.1.2-SNAPSHOT"
  :dependencies [[org.clojure/clojure "1.7.0"]
                 [org.clojure/clojurescript "1.7.122"]
                 [reagent "0.5.1"]
                 ;;Ring is needed to start an HTTP server
                 [ring "1.4.0"]
                 ;;provides default middleware for Ring
                 [ring/ring-defaults "0.1.5"]
                 ;; Routing library
                 [compojure "1.4.0"]
                 ;;server-side HTML templating
                 [hiccup "1.0.5"]
                 ;;thread pool management
                 [aleph "0.4.0"]
                 ;; Dynamic Routing
                 [secretary "1.2.3"]
                 ;; Nice interaction between clj-js
                 [cheshire "5.5.0"]
                 ;; Environment variables
                 [environ "1.0.2"]]

  :main         reagent-server-rendering.handler
  :source-paths ["src/clj"]

  :min-lein-version "2.5.3"

  :uberjar-name "reagent-server-rendering.jar"
  :profiles     {:uberjar      {:source-paths ^:replace ["src/clj"]
                                :hooks        [leiningen.cljsbuild]
                                :omit-source  true
                                :aot          :all
                                :main         reagent-server-rendering.handler
                                :cljsbuild    {:builds {:app {:source-paths ^:replace ["src/cljs"]
                                                              ;;:output-to    "resources/public/js/compiled/app.js"
                                                              :compiler     {:optimizations :advanced
                                                                             :pretty-print false}}}}}}

  :plugins [[lein-cljsbuild "1.0.6"]
            ;;plugin for starting the HTTP server
            [lein-ring "0.9.6"]]

  ;;specifies the HTTP handler
  :ring {:handler reagent-server-rendering.handler/app}

  :clean-targets ^{:protect false} ["resources/public/js/compiled" "target" "test/js"]

  :cljsbuild {:builds {:app {:source-paths ["src/cljs"]
                             :compiler {:output-to "resources/public/js/compiled/app.js"
                                        :optimizations :advanced
                                        :pretty-print false}}}})
