# Experimenting with ISOMORPHIC reagent (CLJ/CLJS)

* No NodeJS
* Seeding initial render state from CLJ to CLJS.

## Development

* Start with `lein run`
* Build an uberjar with `lein uberjar`
* Test the uberjar locally: `heroko local web`

## Deploy to heroku

* One Time configuration: `heroku config:add LEIN_BUILD_TASK="uberjar"`
* Then commit the changes + deploy to Heroku `git push heroku master`

## Sources

* https://github.com/reagent-project/reagent-cookbook/tree/master/recipes/reagent-server-rendering
* https://carouselapps.com/2015/09/14/isomorphic-clojurescriptjavascript-for-pre-rendering-single-page-applications-part-2/
* Isomorphic is the new standard. - https://news.ycombinator.com/item?id=9799333
* Deploy on Heroku - https://jkutner.github.io/2015/10/14/clojurescript-on-heroku.html

## TODOs

* **Evaluate pros and cons for** Server- & Client-Side rendered Routes are duplicated (declared in handler.clj and core.cljs)
* How to populate initial state vs runtime computed state?
